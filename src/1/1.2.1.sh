cd dst
cd d
jq -c '.friends[] |[.id,.name, .knows[1]]' friends.json > task.csv
jq -c '.friends[] |[.id,.name, .knows[0]]' friends.json > task1.csv
cat task.csv task1.csv > task7.csv
sed -i 's/\[//g;s/\]//g;s/,/;/g;s/"//g' task7.csv
cd
cd dst
awk -f ./src/csv-to-tree.awk ./d/task7.csv > task7.dot
dot -Tsvg -O task7.dot
eog ./task7.dot.svg
